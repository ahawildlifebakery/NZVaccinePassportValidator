# NZ Vaccine Passport Validator

## What does it do?

Simple New Zealand vaccine pass verifier, designed to work on a windows computer with a 2D scanner attached via serial interface on the USB port. Whenever a vaccine pass is scanned, it reads and verifies it, and displays its content on the screen.

## Advantages

* fast - as fast as the hardware 2D scanner used
* if the 2D scanner is placed next to the payment terminal, it's all self service and very much self-explaining for the customer
* if the laptop screen is placed so that the cashier can see the content, all vaccine passport data is instantly in sight
* self-service means no extra person for scanning passports needed
* no time lost for the cashier
* no necessity to purchase tablets (but a 2D scanner is needed!)
* low maintenance: perfectly suitable for 1-person operation of a shop
* works without internet connection (only occasionally the internet is required)

## Architecture

![Architecture Diagram][arch]

This application is intended to read, verify and display vaccine passes and their content. It is intended to be used in conjunction with a POS barcode scanner with 2D barcode (QR code) decoding capabilities.

Flow: 

1. POS Barcode Scanner scans/reads QR code and converts it to text
1. POS Barcode Scanner transmits text via COM port to computer with this application installed
1. Application decodes the content (= name, dob, expiration date, ...) and verifies its signature
1. Application displays pass information along with the information if the verification was successful

Further verification (like with a visual ID) may still be necessary.

## Installation (Windows)

* Install Windows Terminal from Windows Store
* Install Python 3.9 from Windows Store
* Open Powershell
* Clone this repo
* Create venv: `python3.9 -m venv venv`
* With current Win10 versions, modify the script execution policy https://docs.microsoft.com/en-gb/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.2
* Activate venv: `venv\scripts\activate`
* Install requirements: `pip install -r requirements.txt`
* Copy config.sample.ini to config.ini and edit
* Create desktop shortcut with target: `wt new-tab Powershell -c <path_to_python_script>\launch.ps1` (replace paths for windows terminal; just type 'wt' and for the launch script)

## Launch

Launch by using the desktop shortcut, or by executing `python scan.py` in the Python 3.9 venv

## System requirements

* Windows 10 system
* Admin permissions
* 2D Scanner configured as "USB-CDC" with "newline" configured after each scan
* Internet is required for installation and updates of the government certificates, - once every 2 weeks.

The "Zebra DS9308" is tested as a 2D scanner and works with paper, mobile phone screens and smartwatches. Other scanners with USB-CDC should work as well. We did not use the "USB Keyboard" mode of the scanner, because it was significantly slower than the serial mode.

## Shortcuts

### Tests

These test shortcuts simulate the hardware scanner and check text content of a QR code.

* f - simulate a failed scan (change simulated QR code content in config.ini)
* t - simulate a successful scan (change simulated QR code content in config.ini)

## Signage

Our signage is a simple .gif file in Full HD resolution (1920x1080px), which includes graphics from the government. The signage has been well tested since the first day of the traffic light system.

<img src="doc/pic/vaccine-pass-signage-without-logo.gif" style="width:75%" >

## Contributing & feedback

We are aware of the fact that our application may have bugs and is certainly not perfectly coded. It is meant as a starting point; we happily accept merge requests, bug reports and code contributions!

## Alternative solutions

* [Official vaccine pass verifier](https://www.health.govt.nz/our-work/diseases-and-conditions/covid-19-novel-coronavirus/covid-19-vaccines/my-covid-record-proof-vaccination-status/nz-pass-verifier) from the NZ Ministry of Health
* [JsNzCovidPassVerifier](https://github.com/aha-aha-aha/JsNzCovidPassVerifier) - offline browser solution
* [NZ My Vaccine Pass Validator list](https://github.com/nz-covid-pass/validator-list)

## References, license, disclaimers and hints

This application has been built 
* to show how fast and easy handling vaccine passes can be
* to appreciate that the government has built something with a reasonable, public specification
* to show that anyone can verify vaccine passes and there is no magic behind
* for daily use in our shop
* and to help fighting the pandemic.

The application, including its source code is available for commercial and private use without charge. It attempts to implement the vaccine passport specification from the NZ Ministry of Health (https://nzcp.covid19.health.nz/). The application comes without any warranty or guarantees; use it at your own risk! The NZ government recommends the use of the government approved vaccine pass verifier app instead (https://www.health.govt.nz/our-work/diseases-and-conditions/covid-19-novel-coronavirus/covid-19-vaccines/my-covid-record-proof-vaccination-status/nz-pass-verifier). 

Licenses:

* Error sound from: https://www.sfxbuzz.com/summary/11-buzzer-alarm-and-siren-sound-effects/156-wrong-answer-sound-effect (CC licensed; converted to wav)
* Success sound https://www.freesoundslibrary.com/ding-dong-sound-effect/ (CC BY 4.0 licensed; converted to wav)


[arch]: doc/pic/vaccine-pass-verifier-diagram.png
