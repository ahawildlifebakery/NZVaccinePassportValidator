from genericpath import exists
import serial
import time
import configparser
import math
import base64
import logging
from cose.keys.ec2 import EC2
from cose.keys.keyparam import KpKty, EC2KpCurve, EC2KpX, EC2KpY
from cose.keys.ec2 import EC2Key
from cose.messages import Sign1Message, CoseMessage
from cose.keys import CoseKey
from cose import headers
import cose
import urllib.request, json 
from cose.keys.keytype import KtyEC2
from cose.keys.curves import P256, CoseCurve
from cose.keys.keytype import KtyOKP
from cose.keys.keyparam import KpKty, OKPKpD, OKPKpX, KpKeyOps, OKPKpCurve
from cose.keys.keyops import SignOp, VerifyOp
from cose.messages import Sign1Message, CoseMessage
from cose.keys import CoseKey
import cose.keys.keytype
from cose.headers import Algorithm, KID
from cose.algorithms import EdDSA
from cose.keys.curves import Ed25519
import cose.keys.curves
from cose.keys.keyparam import KpKty, OKPKpD, OKPKpX, KpKeyOps, OKPKpCurve
from cose.keys.keytype import KtyOKP
from binascii import unhexlify
from cose.keys.keyops import SignOp, VerifyOp
import os
from datetime import datetime
from os import path
from opencensus.ext.azure.log_exporter import AzureLogHandler, AzureEventHandler
import PySimpleGUI as sg
from  cbor2 import loads
from datetime import date


# read configuration from file
config = configparser.ConfigParser()
config.read('config.ini')
append_str_to_simulated_codes = '..' # 2 characters need to be appended to simulated scan codes
verify_signature_mode = config['general'].getboolean('verify_signatures')

# initialize logging
logger = logging.getLogger(__name__)
debug_mode = config['general'].getboolean('debug_mode')
if not debug_mode: os.system('cls')

if (config['logging'].getboolean('azure_monitor_application_insights_enabled')):
    instrumentation_key = config['logging']['azure_monitor_instrumentation_key']
    logger.addHandler(AzureLogHandler(connection_string=f'InstrumentationKey={instrumentation_key}'))
    logger.addHandler(AzureEventHandler(connection_string=f'InstrumentationKey={instrumentation_key}'))

logger.info("Vaccine Pass Scanner starting up")
if not os.path.exists('log'):
    os.mkdir('log')
if not os.path.exists('crtcache'):
    os.mkdir('crtcache')
logging.basicConfig(filename=f'log/debug-{datetime.now().strftime("%Y_%m_%d")}.log', encoding='utf-8', level=logging.DEBUG, format='%(asctime)s %(message)s')


def play_alert(success=True):
    if config['general'].getboolean('sound_enabled'):
        try:
            from pygame import mixer
            mixer.init() 
            if success == False:
                sound=mixer.Sound("sound/error.wav")
            else:
                sound=mixer.Sound("sound/success.wav")
            sound.play()
        except Exception as e:
            print("Sound Error")
            logger.exception(f"Error playing audio file")
            
def scanit(qr_code_content):
    if not debug_mode: os.system('cls')

    qr_code_parts = qr_code_content.split('/')
    pass_type = qr_code_parts[0]
    pass_protocol_version = qr_code_parts[1]
    pass_content = qr_code_parts[2]

    result = {}
    result['pass_errors'] = []
    result['accepted'] = True
    result['person'] = {}

    if debug_mode: print("*** Part 1: Parsing the QR Content")
    if debug_mode: print('Pass Type:', pass_type)
    if debug_mode: print('Protocol Version:', pass_protocol_version)
    if debug_mode: print('Pass Content:', pass_content)

    if debug_mode: print("\n*** Part 2: Decoding the QR Content (base32)")
    pad_length = math.ceil(len(pass_content) / 8) * 8 - len(pass_content)
    if debug_mode: print("Pad Length:", pad_length)
    output_bytes_data = base64.b32decode(pass_content.encode('ascii') + b'=' * pad_length)
    if debug_mode: print("Output Bytes Data:", output_bytes_data)

    if debug_mode: print("\n*** Part 3: Cose decoding")
    cose_decoded = CoseMessage.decode(output_bytes_data)

    if debug_mode: print("cbor tag:", cose_decoded.cbor_tag)
    if debug_mode: print("uhdr:", cose_decoded.uhdr)
    if debug_mode: print("phdr:", cose_decoded.phdr)
    kid = cose_decoded.phdr.get(cose.headers.KID).decode('utf-8')
    if debug_mode: print('kid:', kid)
    if debug_mode: print('algorithm:', cose_decoded.phdr.get(cose.headers.Algorithm))
    if debug_mode: print("payload:", cose_decoded.payload)
    if debug_mode: print("signature:", cose_decoded.signature)

    if debug_mode: print("\n*** Part 4: Decoding payload (CBOR)")
    obj = loads(cose_decoded.payload)
    if debug_mode: print("decoded payload:", obj)
    exp_date = datetime.fromtimestamp(obj[4])
    if exp_date < datetime.now():
        result['accepted'] = False
        result['pass_errors'].append(pass_error(f"Passport expired on {exp_date}", qr_code_content))

    valid_date = datetime.fromtimestamp(obj[5])
    if valid_date > datetime.now():
        print("ERROR:", "Passport only valid from", valid_date)
        result['accepted'] = False
        result['pass_errors'].append(pass_error(f"Passport not valid yet (only from {valid_date})", qr_code_content))

    person = obj['vc']['credentialSubject']
    print(f"First Name: {bcolors.OKGREEN}{person['givenName']}{bcolors.ENDC}")
    print(f"Last Name:  {bcolors.OKGREEN}{person['familyName']}{bcolors.ENDC}")
    print(f"DoB:        {bcolors.OKGREEN}{person['dob']}{bcolors.ENDC}")
    person['age'] = calculate_age(person['dob'])
    print(f"Age:        {bcolors.OKGREEN}{person['age']}{bcolors.ENDC}")
    result['person'] = person
    if config['data'].getboolean('trace_age'):
        logger.info("Pass decoded successfully", extra={'custom_dimensions': {'age': person['age']}})
    
    if verify_signature_mode:
        issuer = obj[1]
        if debug_mode: print("\n*** Part 5: Downloading issuer certificates")
        issuer_domain = issuer.split(':')[2]
        if debug_mode: print("issuer domain:", issuer_domain)
        
        refresh_cert_file = False
        old_cert_file_exists = False
        cert_file = f"crtcache/{issuer_domain}"
        if not os.path.exists(cert_file):
            refresh_cert_file = True
        else:
            old_cert_file_exists = True
            file_timestamp = os.path.getmtime(cert_file)
            age = time.time()-file_timestamp
            if age > 60*1: #60*24:
                if debug_mode: print("Certificate file is too old. Will try to download a new one.")
                logger.info(f"Certificate file is {age} seconds old; renewal required.")
                refresh_cert_file = True

        if refresh_cert_file:
            try:
                json_url = f"https://{issuer_domain}/.well-known/did.json"
                if debug_mode: print('cert url:', json_url)
                with urllib.request.urlopen(json_url) as url:
                    with open(cert_file, "wb") as file:
                        file.write(url.read())
                        logger.info("New certificate file downloaded successfully.")
            except Exception as e:
                print("Download Error")
                logger.exception(f"Error downloading certificate file {json_url}")
                if old_cert_file_exists:
                    if debug_mode: print("### Error downloading the government's certificate. Proceeding with old file.")
                else:
                    if debug_mode: print("### Error downloading the government's certificate. Proceeding without validation.")
                    return result

        with open(cert_file, "r") as file:
            issuer_cert_data = json.load(file)
            if debug_mode: print('cert data:', issuer_cert_data)

        assertion_method_found = False
        for asm in issuer_cert_data['assertionMethod']:
            if debug_mode: print(f"{issuer}#{kid}", asm)
            if f"{issuer}#{kid}" == asm:
                assertion_method_found = True
        if assertion_method_found == False:
            result['pass_errors'].append(pass_error(f"ERROR: Key {kid} is not approved by the government.", qr_code_content))
            print("Error: Assertion Method not found in certificate")
            result['accepted'] = False
            return result

        verification_method_found = False
        for verification_method in issuer_cert_data['verificationMethod']:
            if verification_method['id'] != f"{issuer}#{kid}":
                if debug_mode: print("DIFF", "id")
                continue
            if not config['general'].getboolean('accept_demo_passports', False) and verification_method['controller'] == 'did:web:nzcp.covid19.health.nz':
                result['accepted'] = False
                result['pass_errors'].append(pass_error(f"This is a demo passport and not accepted. ", qr_code_content))
                continue
            if not config['general'].getboolean('accept_demo_passports', False) and verification_method['controller'] != 'did:web:nzcp.identity.health.nz':
                if debug_mode: print("DIFF", "controller")
                continue
            if config['general'].getboolean('accept_demo_passports', False) and verification_method['controller'] != 'did:web:nzcp.identity.health.nz' and verification_method['controller'] != 'did:web:nzcp.covid19.health.nz':
                if debug_mode: print("DIFF", "controller")
                continue
            if verification_method['type'] != 'JsonWebKey2020':
                if debug_mode: print("DIFF", "type")
                continue
            jwk_key = verification_method['publicKeyJwk']
            verification_method_found = True
        if verification_method_found == False:
            print("Error: Verification method not found in certificate")
            result['pass_errors'].append(pass_error(f"Verification method not found in certificate.", qr_code_content))
            result['accepted'] = False
            return result
        if debug_mode: print("JWK Key:", jwk_key)

        print("\n*** Part 6: Verifying certificate")
        x = jwk_key['x']
        if len(x) % 4:
            x = x + "=" * (4 - len(x)%4)
        ec_x = base64.urlsafe_b64decode(x)

        y = jwk_key['y']
        if len(y) % 4:
            y = y + "=" * (4 - len(y)%4)
        ec_y = base64.urlsafe_b64decode(y)


        key_as_dict = {
            KpKty: cose.keys.keytype.KtyEC2,
            EC2KpCurve: P256,
            EC2Key: kid,
            KpKeyOps: [VerifyOp],
            EC2KpX: ec_x,
            EC2KpY: ec_y,
        }
        govt_key = CoseKey.from_dict(key_as_dict)
        cose_decoded.key = govt_key
        if debug_mode: print("Decoded key:", govt_key)
        verification_result = cose_decoded.verify_signature()
        print("Verification result:", verification_result)
        if verification_result == False:
            result['pass_errors'].append(pass_error(f"Signature cannot be verified.", qr_code_content))
            result['accepted'] = False

    return result

def show_passport_content(errors, person, accepted=False):
    err_str = ""
    for e in errors:
        err_str += f"{e.error_message}\n"
    window["-TERR-"].update(f"{err_str}")
    window["-TOUT-"].update(f"{person['givenName']}\n{person['familyName']}\n{person['age']}")

    verification_color = 'green'
    if accepted == False:
        verification_color = 'red'
        print(f"{bcolors.WARNING}ERROR{bcolors.ENDC}")
        play_alert(False)
    else:
        play_alert()

    window["-TOUT-"].update(background_color=verification_color)
    window["-TERR-"].update(background_color=verification_color)

    # Bring window to top
    window.BringToFront()
    window.TKroot.attributes('-topmost', True)
    window.TKroot.attributes('-topmost', False)

class pass_error:
    fatal = True
    error_message = ''

    def __init__(self, error_message, scanned_code, fatal = True):
        self.fatal = fatal
        self.error_message = error_message
        print("ERROR:", error_message)
        logger.exception(f"Passport ERROR: {error_message}", extra={'custom_dimensions': {"barcode": scanned_code, "errormessage": error_message}})

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def calculate_age(dob):
    ### returns the age in years
    ### expects the dob in format yyyy-mm-dd
    bdyear = int(dob[0:4])
    bdmonth = int(dob[5:7])
    bdday = int(dob[8:10])
    today = date.today()
    age = today.year - bdyear - ((today.month, today.day) < (bdmonth, bdday))
    return age

# Connect to the scanner, which should be found at the configured COM port
try:
    if config['scanner']['comport'] != "none":
        serialPort = serial.Serial(
            port=config['scanner']['comport'], baudrate=115200, bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE
        )
except:
    print("ERROR: No connection to scanner on", config['scanner']['comport'])
    logger.exception(f"ERROR: No connection to scanner.")
    exit()
serialString = ""

font = ("Arial", 41)
font_err = ("Arial", 12)
layout = [[sg.Text("Please scan!", size=(35, 3), font=font, key="-TOUT-")], [sg.Text(":)", size=(60, 8), font=font_err, key="-TERR-")], [sg.Button("CLOSE")]]
window = sg.Window("NZ Vaccine Passport Reader", layout, return_keyboard_events=True, finalize=True)
# Bring window to top
window.BringToFront()
window.TKroot.attributes('-topmost', True)
window.TKroot.attributes('-topmost', False)

print("Ready to scan!")
last_scan_time = None

def scancode(serialString):
    # Print the contents of the serial data
    try:
        scanned_code = serialString.decode("Ascii")[0:-2]
        scan_result = scanit(scanned_code)
        show_passport_content(scan_result['pass_errors'], scan_result['person'], accepted=scan_result['accepted'])

    except Exception as e:
        if debug_mode: print("Exception:", e)
        logger.exception("Passport not recognized", extra={'custom_dimensions': {"barcode": scanned_code}})
        print(f"{bcolors.WARNING}ERROR{bcolors.ENDC}")
        window["-TOUT-"].update('ERROR / Exception')
        window["-TERR-"].update(e)
        window["-TOUT-"].update(background_color='red')
        window["-TERR-"].update(background_color='red')
        play_alert(False)
        pass
    
play_alert()
# main loop
while 1:
    if last_scan_time and time.time() - last_scan_time > 180:
        last_scan_time = None
        window["-TOUT-"].update(background_color='black')
        window["-TOUT-"].update("Ready to scan!")
        window["-TERR-"].update(background_color='black')
        window["-TERR-"].update(":)")
        if not debug_mode: os.system('cls')
    try:
        event, values = window.read(timeout=1)
        if event == "CLOSE" or event == sg.WIN_CLOSED:
            break
        if len(event) == 1:
            if event == "t":
                print("Test (success)!")
                last_scan_time = time.time()
                scancode(bytes(f"{config['testmode']['successful_input']}{append_str_to_simulated_codes}", 'utf-8'))
            if event == "f":
                print("Test (fail)!")
                last_scan_time = time.time()
                scancode(bytes(f"{config['testmode']['failed_input']}{append_str_to_simulated_codes}", 'utf-8'))
    except:
        time.sleep(0.01) 
        pass

    # Wait until there is data waiting in the serial buffer
    if config['scanner']['comport'] != "none" and serialPort.in_waiting > 0:

        # Read data out of the buffer until a carraige return / new line is found
        serialString = serialPort.readline()
        # store the current scan time so we can hide the result after a timeout
        last_scan_time = time.time()
        scancode(serialString)    
        
    # The "sleep" ensures that the program remains responsive while waiting for input
    # from the scanner and/or the GUI.
    time.sleep(0.01) 

window.close()